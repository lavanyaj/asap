#include "Asap.h"
#include "assert.h"
#include <iostream>

#define LOG 1
#define REMARK_ON_FORWARD 0
#define REMARK_ON_REVERSE 1

void Events::destinationHandleForward(
	   Message& message, Hop& hop) {

  Message::Frm& FRM = message.frm;
  Message::Brm& BRM = message.brm;
  BRM.BID = 0;
  BRM.ER = FRM.ER; // always MAX_RATE
  BRM.HopCount = FRM.HopCount;

  if (REMARK_ON_REVERSE)
    BRM.ACR = FRM.ACR;

  message.kind = Message::MessageKind::BRM;
  message.frm = {};

}

void Events::sourceCreate(
	   Message& message, Hop& hop) {

  // changes to per-flow state in source
  int flowId = message.getFlowId();
  Message::Init& init = message.init;
  Message::Frm& RM = message.frm;

  
  Hop::SourceFlowState& flowState = hop.source[flowId];
  flowState.BID = 0;
  flowState.OldBID = 0;
  flowState.ACR = init.ICR;
  flowState.OldACR = 0;
  //flowState.PCR = inputMessage.PCR; PCR is some constant MAX_RATE

  // TODO(lav): BID is -2 or flowState.BID = 0??
  // OldBID and OldACR??
  RM.HopCount = 0;
  RM.BID = -2;
  RM.ACR = flowState.ACR;
  RM.ER = MAX_RATE; //flowState.PCR;
  
  // default values for sending FRM
  RM.OldBID = flowState.OldBID;
  RM.OldACR = flowState.OldACR;
  message.kind = Message::MessageKind::FRM;
  message.init = {};
  
  // update source variables
  flowState.OldBID = RM.BID;
  flowState.OldACR = RM.ACR;
}

void Events::sourceHandleTerminate(
	   Message& message, Hop& hop) {

  int flowId = message.getFlowId();
  assert(hop.source.count(flowId) > 0);
  Hop::SourceFlowState& flowState = hop.source.at(flowId);

  Message::Frm& RM = message.frm;

  // special values for terminate
  RM.BID = -1;
  RM.OldBID = flowState.OldBID;
  RM.OldACR = flowState.OldACR;
  
  // default values for sending FRM
  RM.HopCount = 0;
  RM.ACR = flowState.ACR;
  RM.ER = MAX_RATE; //flowState.PCR;
  message.kind = Message::MessageKind::FRM;

  
}

void Events::sourceHandleReverse(
	   Message& message, Hop& hop) {

  // changes to per-flow state in source
  int flowId = message.getFlowId();
  Message::Brm& BRM = message.brm;
  Message::Frm& FRM = message.frm;
  assert(hop.source.count(flowId) > 0);
  Hop::SourceFlowState& flowState = hop.source.at(flowId);
  flowState.BID = BRM.BID;

  assert(flowState.BID != 0); // some hop updated it on the way
  //if (BRM.ER < flowState.MCR) flowState.BID = -3;
  //flowState.ACR = std::max(flowState.MCR, std::min(BRM.ER,
  //						   flowState.PCR));

  flowState.ACR = BRM.ER;
  if (MAX_RATE < BRM.ER) flowState.ACR = MAX_RATE;
 
  // default values for sending FRM
  FRM.HopCount = 0;
  FRM.ER = MAX_RATE; //flowState.PCR;
  
  FRM.BID = flowState.BID;
  FRM.ACR = flowState.ACR;

  FRM.OldBID = flowState.OldBID;
  FRM.OldACR = flowState.OldACR;

  message.kind = Message::MessageKind::FRM;
  message.brm = {};
  
  // update source variables
  flowState.OldBID = FRM.BID;
  flowState.OldACR = FRM.ACR;
}

void Events::linkHandleForward(
	   Message& message, Hop& hop) {
  // only updates hop (N, Nu, Fc, R) not message (except hopCount)!
  
  Message::Frm& RM = message.frm;
  Hop::Link& link = hop.link;
  
  RM.HopCount++;
  // New flows
  if (RM.BID == -2) {
    link.N++;
    // ASAP paper just updates N. R as if unconstrained. 
    // Note ASAP tracks Nu at switch.
    //  Constrained/ unconstrained set only on second FRM
    // First BRM is bn with R = min C / N
    // R is updated with second FRM - constrained (cuz OldBID = 0)
    //  to unconstrained, so Fc = 0,  Nu+= 1 <strikeout> Nc -= 1 (!?) <\strikeout>,
    //    and R = C / N (1 flow, 1 link case)
    link.R = link.C/link.N;
    // TODO(lav): corrected to treat flow as sat, update Fc to include 
    // initRate
    // so next time at bn switch const -> unconst,
    //  Fc -= initRate is correct, and other switches
    //  Fc -= initRate + min of Rs also correct
    link.Fc += RM.ACR;


    // ASAP MR paper updates Nc, Fc also, so treating as constrained on first FRM.
    // ASAP MR tracks Nc at switch  
    //  But no change to R, first BRM is bn with R = 0.
    // Then R is updated with second FRM - constrained (cuz OldBID = 0)
    // to unconstrained, so Fc = 0 Nc -= 1
    // and R = C / N (1 flow, 1 link case)  

    //link.Nc++;
    //link.Fc = link.Fc + RM.ACR;

    return;
  }

  if (LOG) std::cout << "update N, Fc, Nu";
  // update N, Fc, Fm, Nu, Nm

  if (RM.BID == -1) {   // if flow's quitting
    // if unconstrained
    if (RM.OldBID == RM.HopCount) {
      link.N--;
      link.Nu--;
      if (LOG) std::cout << " unconstrained -> quitting";
    } else {
      // if constrained
      link.N--;
      link.Fc = link.Fc - RM.OldACR;
      if (LOG) std::cout << " constrained ("
		<< RM.OldACR
		<< ") -> quitting";
    }
    
  } else if (RM.BID == RM.HopCount) { // if flow becomes unconstrained
    // if was constrained
    if (RM.OldBID != RM.HopCount) { // also true for new flow
      if (LOG) std::cout << " constrained ("
		<< RM.OldACR
		<< ") -> unconstrained";
      link.Nu++;
      link.Fc -= RM.OldACR;
    } else { // else no updates
      if (LOG) std::cout << " unconstrained -> unconstrained";
    }
  
  } else { // if flow becomes constrained
    // if flow was unconstrained
    if (RM.OldBID == RM.HopCount) {
      link.Nu--;
      link.Fc += RM.ACR;
      if (LOG) std::cout << " unconstrained -> constrained";
      // if flow was constrained
    } else {
      link.Fc = link.Fc - RM.OldACR + RM.ACR;
      if (LOG) std::cout << " constrained ("
		<< RM.OldACR
		<< ") -> constrained ("
		<< RM.ACR << ")";
    } 
  } // ends if flow becomes constrained
    
    // update R
  if (LOG) std::cout << "\nupdate R";
  // if there are unconstrained VCs
  if (LOG) std::cout << " unconstrained: " << link.Nu;
  if ( link.Nu > 0) {
    double rem = (link.C - link.Fc);
    if (LOG) std::cout << " rem: " << rem;
    link.R = rem/link.Nu;
    if (LOG) std::cout << " link.R: " << link.R;
      if (LOG) std::cout << "\n";
  // if all VCs are constrained
  } else {
    if (LOG) std::cout << " link.Fc: " << link.Fc;
    if (LOG) std::cout << " link.C: " << link.C;
    if (link.Fc > link.C) {
      if (LOG) std::cout << "link.N: " << link.N;
      assert(link.N > 0);
      if (LOG) std::cout << " reset R";
      // Reset R
      link.R = link.C/link.N;
      if (LOG) std::cout << " link.R: " << link.R;
    }
  if (LOG) std::cout << "\n";
  }

  // re-marking done as last step on receiving FRM??
  if (REMARK_ON_FORWARD) {
  if (LOG) std::cout << "\nCheck for re-marking";
  if (RM.BID != RM.HopCount && RM.BID != -1 && RM.BID != -3 
      && RM.ACR >= link.R) {
    if (LOG) std::cout << ". Constrained by ACR " << RM.ACR 
		       << " is larger than new R " << link.R;
    double RTemp = (link.C - link.Fc + RM.ACR)/(link.Nu+1);
    if (LOG) std::cout << ". RTemp is " << (link.C - link.Fc + RM.ACR) 
		       << "/ " << (link.Nu + 1);
    if (RM.ACR >= RTemp) {
      RM.BID = RM.HopCount;
      RM.ER = RTemp;
      if (LOG) std::cout  << ". Remark flow as unconstrained here"
			  << " and set ER to " << RTemp;
    }
  } else { if (LOG) std::cout << ". Not needed"; 
  }
  if (LOG) std::cout << "\n";
  }
}

void Events::linkHandleReverse(
			       Message& message, Hop& hop) {
  // only updates message (ER, BID, HopCount), not hop!
  
  Message::Brm& RM = message.brm;
  Hop::Link& link = hop.link;

  if (RM.ER > link.R) {
    RM.ER = link.R;
    RM.BID = RM.HopCount;
  }

  if (REMARK_ON_REVERSE) {
  if (LOG) std::cout << "\nCheck for re-marking";
  if (RM.BID != RM.HopCount && RM.BID != -1 && RM.BID != -3 
      && RM.ACR >= link.R) {
    if (LOG) std::cout << ". Constrained by ACR " << RM.ACR 
		       << " is larger than new R " << link.R;
    double RTemp = (link.C - link.Fc + RM.ACR)/(link.Nu+1);
    if (LOG) std::cout << ". RTemp is " << (link.C - link.Fc + RM.ACR) 
		       << "/ " << (link.Nu + 1);
    if (RM.ACR >= RTemp) {
      RM.BID = RM.HopCount;
      RM.ER = RTemp;
      if (LOG) std::cout  << ". Remark flow as unconstrained here"
			  << " and set ER to " << RTemp;
    }
  } else { if (LOG) std::cout << ". Not needed"; 
  }
  if (LOG) std::cout << "\n";
  }

  RM.HopCount--;
}

void Events::handleEvent(Message& message, Hop& hop) {
  if (hop.getKind() == Hop::HopKind::SOURCE) {
    if (message.kind == Message::MessageKind::INIT) {
      if (LOG) std::cout << "**sourceCreate "
		<< message.getFlowId() << "**\n";
      sourceCreate(message, hop);
    } else if (message.kind == Message::MessageKind::BRM) {
      if (LOG) std::cout << "**sourceHandleReverse "
		<< message.getFlowId() << "**\n";
      sourceHandleReverse(message, hop);
    } else {
    }
  } else if (hop.getKind() == Hop::HopKind::LINK) {
    if (message.kind == Message::MessageKind::FRM) {
      if (LOG) std::cout << "**linkHandleForward "
		<< message.getFlowId() << "**\n";

      linkHandleForward(message, hop);
    } else if (message.kind == Message::MessageKind::BRM) {
      if (LOG) std::cout << "**linkHandleReverse "
		<< message.getFlowId() << "**\n";

      linkHandleReverse(message, hop);
    } else {
    }
    } if (hop.getKind() == Hop::HopKind::DESTINATION) {
    if (message.kind == Message::MessageKind::FRM) {
       if (LOG) std::cout << "**destinationHandleForward "
		<< message.getFlowId() << "**\n";

      destinationHandleForward(message, hop);
    } else {
    }
    }


}

std::string Hop::getString() const {    
    if (kind == HopKind::LINK) return link.getString();
      std::stringstream ss;
      if (kind == HopKind::SOURCE) {
	ss << "SOURCE ";
      for (const auto& flowState: source)
	ss << "\n" << flowState.first
	   << ": " << flowState.second.getString();
      } else if (kind == HopKind::DESTINATION) {
	ss << "DESTINATION ";
      }
    return ss.str();
  }

std::string Hop::Link::getString() const {
    std::stringstream ss;
    ss << "LINK-"
      << " C : " << C
      << ", Fc: " << Fc
       << ", N: " << N
      << ", Nu: " << Nu
       << ", R: " << R;
    return ss.str();
  }

std::string Hop::SourceFlowState::getString() const {
    std::stringstream ss;
    ss << "SourceFlowState-"
      << " BID : " << BID
      << ", OldBID: " << OldBID
       << ", ACR: " << OldACR;
    return ss.str();
  }

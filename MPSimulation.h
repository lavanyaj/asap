#ifndef __MP_SIMULATION_H
#define __MP_SIMULATION_H

#include "Asap.h" // Message, Hop
#include <vector>


class MPSimulation {
  std::vector<Hop> hops;
  const std::vector<std::vector<int> > paths;
  const std::vector<int> flowToPath;
  const int numFlows;

  // of size numFlows, indexed by flow number
  std::vector<Message> flowMessage;
  std::vector<int> flowNextHop;

 public:
 MPSimulation(std::vector<Hop>& hops,
	      std::vector<std::vector<int> >& paths,
	      std::vector<int>& flowToPath) :
  hops(hops),
  paths(makeRoundTrip(paths)),
    flowToPath(flowToPath),
    numFlows(flowToPath.size()) {
    // TODO(lav): validate paths
    // TODO(lav): validate flow to path index
    // set up messages
    // set up next hop index
    for (int i = 0; i < numFlows; i++) {
      flowMessage.push_back(Message(i, Message::MessageKind::INIT));
      flowMessage.back().init.ICR = 1;
      flowNextHop.push_back(0);
    }
  }

  void run(int maxSteps, int seed); // specify seed -1 for deterministic

  std::vector<std::vector<int> > makeRoundTrip
    (const std::vector<std::vector<int> >& paths);

  // run till you get same message type for each flow
  // check if messages match up, revert to original
  bool converged();
};

#endif // __MP_SIMULATION_H

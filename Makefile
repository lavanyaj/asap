OBJS = asap.o asap-test.o  workload.o

CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG) -std=c++11
LFLAGS = -Wall $(DEBUG)



Asap.o: Asap.h Asap.cc
	$(CC) $(CFLAGS) Asap.cc

Workload.o: Workload.h Workload.cc Asap.h
	$(CC) $(CFLAGS) Workload.cc

MPSimulation.o: MPSimulation.h MPSimulation.cc Asap.h Workload.h
	$(CC) $(CFLAGS) MPSimulation.cc 

AsapTest.o : Asap.h Asap.cc AsapTest.cc
	$(CC) $(CFLAGS) AsapTest.cc

AsapTest : Asap.o AsapTest.o
	$(CC) $(LFLAGS) AsapTest.o Asap.o -o AsapTest

WorkloadTest.o : Workload.h WorkloadTest.cc
	$(CC) $(CFLAGS) WorkloadTest.cc

WorkloadTest : Workload.o WorkloadTest.o Asap.o
	$(CC) $(LFLAGS) Workload.o WorkloadTest.o Asap.o -o WorkloadTest

MPSimulationTest.o : MPSimulation.h MPSimulationTest.cc Asap.h Workload.h
	$(CC) $(CFLAGS) MPSimulationTest.cc

MPSimulationTest: Workload.o MPSimulation.o Asap.o MPSimulationTest.o
	$(CC) $(LFLAGS) Workload.o MPSimulation.o MPSimulationTest.o Asap.o -o MPSimulationTest

all: AsapTest WorkloadTest MPSimulationTest

clean:
	\rm *.o *~ p1

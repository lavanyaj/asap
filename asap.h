#ifndef __ASAP_H
#define __ASAP_H

#include <assert.h>
#include <string>
#include <sstream>
#include <map>
#include <vector>

#define MAX_RATE 1000



// could be source destination or link
class Hop {
public:
  enum class HopKind {SOURCE, LINK, DESTINATION, INVALID};

  // SOURCE
  class SourceFlowState {
  public:
  int BID;
  int OldBID;
  double ACR;
  double OldACR;

  SourceFlowState() : BID(0), OldBID(0), ACR(0), OldACR(0) {};
  std::string getString() const;
  };

  // DESTINATION (no state)
  class Link {
  public:
  // LINK
  double C;
  double Fc; // constrained VCs
  int N; // number of VCs
  int Nu;
  double R; // advertised rate
  Link() : C(0), Fc(0), N(0), Nu(0), R(0) {};
  std::string getString() const;
  };

  class DestinationFlowState {
  };

  typedef std::map<int, SourceFlowState> Source;
  typedef std::map<int, DestinationFlowState> Destination;

  Source source;
  Link link;
  Destination destination;

 Hop() : kind(HopKind::INVALID) {
  };
  
 Hop(HopKind kind) : kind(kind) {    
    assert(kind != HopKind::LINK);
  };

 Hop(HopKind kind, double C) : kind(kind) {
    assert(kind == HopKind::LINK);
    link = {};    
    link.C = C;    
  };

  HopKind getKind() { return kind;};
  std::string getString() const;
  // need to make sure all fields initialized
  private:
    HopKind kind;
};



// could be forward or reverse or init info
class Message {
private:
  int flowId;

public:
  enum class MessageKind {INIT, FRM, BRM, INVALID};

  class Init {
  public:
    //double PCR;
    //double MCR;
    //double ACR;
    double ICR;

    bool equals(const Init& other) const {
      return (ICR == other.ICR);
    }

      std::string getString() const {
    std::stringstream ss;
    ss << "Init -"
       << " ICR : " << ICR;
    return ss.str();
  }

  };
  
 class Frm {
 public:
  int BID;
  int OldBID;
  double ACR;
  double OldACR;
  double ER;
  int HopCount;

  bool equals(const Frm& other) const {
    return (BID == other.BID && OldBID == other.OldBID
	    && ACR == other.ACR && OldACR == other.OldACR
	    && ER == other.ER && HopCount == other.HopCount);
  }

    std::string getString() const {
    std::stringstream ss;
    ss << "FRM -"
      << " BID: " << BID
      << ", OldBID: " << OldBID
      << ", ACR: " << ACR
      << ", OldACR: " << OldACR
      << ", ER: " << ER
       << ", HopCount: " << HopCount;
    return ss.str();
  }

 };

class Brm {
 public:
  int BID;
  double ER;
  double ACR; // only when re-marking done on reverse
  int HopCount;

  bool equals(const Brm& other) const {
    return (BID == other.BID && ER == other.ER 
	    && ACR == other.ACR 
	    && HopCount == other.HopCount);
  }

      std::string getString() const {
    std::stringstream ss;
    ss << "BRM -"
      << " BID: " << BID
      << ", ER: " << ER
      << ", (ACR: " << ACR << ")"
       << ", HopCount: " << HopCount;
    return ss.str();
  }

};


  MessageKind kind;
  Frm frm;
  Brm brm;
  Init init;

 Message() : flowId(-1), kind(MessageKind::INVALID) {}
 Message(int flowId, MessageKind kind) : flowId(flowId), kind(kind) {    
  }
  bool valid() const { return (kind != MessageKind::INVALID);}

  bool equals(const Message& other) const {
    if (!valid() && !other.valid()) return true;
    if (kind != other.kind) return false;
    if (kind == MessageKind::FRM) return frm.equals(other.frm);
    else if (kind == MessageKind::BRM) return brm.equals(other.brm);
    else if (kind == MessageKind::INIT) return init.equals(other.init);
    assert(false);
    return false;
  } 
  int getFlowId() const {return flowId;}

  std::string getString() const {
    std::stringstream ss;
    ss << "Flow " << flowId << " ";
    if (kind == MessageKind::INIT) ss << init.getString();
    else if (kind == MessageKind::FRM) ss << frm.getString();
    else if (kind == MessageKind::BRM) ss << brm.getString();
    else if (kind == MessageKind::INVALID) ss << " INVALID";
    else ss << " ???";
    return ss.str();
    
  }
  // TODO string <<
  
  // Manually make sure all fields initialized
  // when changing message kind
};



// class of functions of the form (const InputMessage&, const InputHop
// OutputMessage&, OutputHop&)
class Events {
 private:  
  static void sourceCreate(Message& outputMessage, Hop& outputHop);
  static void sourceHandleReverse(Message& outputMessage, Hop& outputHop);
  static void sourceHandleTerminate(Message& outputMessage, Hop& outputHop);

  static void linkHandleForward(Message& outputMessage, Hop& outputHop);
  static void linkHandleReverse(Message& outputMessage, Hop& outputHop);
  static void destinationHandleForward(Message& outputMessage, Hop& outputHop);
 public:
  // Processes message at hop, changing both message and hop
  static void handleEvent(Message& message, Hop& hop);
};

#endif

#include "Workload.h"
#include "MPSimulation.h"
#include <assert.h>
#include <iostream> // std::cout

int main(int argc, char *argv[]) { 
  std::cout << "usage ./MPSimulationTest cap1.txt tm1.txt 200 1" << std::endl;

  Workload w;
  assert(argc >= 4);
  w.parseCapFile(argv[1]);
  w.parseTmFile(argv[2]); 
  
  MPSimulation s(w.getHops(), w.getPaths(), w.getFlowToPathId());;
  if (argc == 5) {
    int seed = atoi(argv[4]);
    s.run(atoi(argv[3]), seed);
  } else s.run(atoi(argv[3]), -1);

  return 0;
}

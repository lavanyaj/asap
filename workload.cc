#include "Asap.h"
#include "Workload.h"

#include <iostream> // cout
#include <fstream> // ifstream
#include <cstring>
#include <assert.h>
#include <stdlib.h> // strtol
#include <vector>
//using namespace::std;



int Workload::parseCapFile(const char* capfile) {

  std::ifstream inFile(capfile);
  std::string oneLine;

  std::cout << "parsing capacity file " << capfile << std::endl;
  if (!inFile.is_open()) return 1;

  while(getline(inFile, oneLine)) {
    // std::cout << "tokenizing " << oneLine << std::endl;

    char cline[80];
    int size = oneLine.length();
    if (80< size) size = 80;

    std::strncpy(cline, oneLine.c_str(), size);

    char delim[] = " \n";
    char *token = NULL;
    int numTokens = 0;

    token = std::strtok(cline, delim);
    // std::cout << token << std::endl;
    numTokens++;


    int id = getId(token);

    while((token=std::strtok(NULL, delim)) != NULL) {
      // std::cout << token << std::endl;
      long cap = strtol(token, NULL, 0);
      hops[id] = Hop(Hop::HopKind::LINK, cap);
      numTokens++;
    }

    // std::cout << std::endl;    
  }
  inFile.close();

  return 0;
}

int Workload::getId(const char* token) {
  // std::cout << "Getting ID for token " << token << std::endl;
  std::string strToken(token);

  // std::cout << "Getting ID for strToken " << strToken << std::endl;
  int id = ids.size();

  assert(strToken.length() > 0);
  assert(strToken[0] == 'S' || strToken[0] == 'L' || strToken[0] == 'D');

  if (ids.count(strToken) == 0) {
    ids[strToken] = id;
    // std::cout << "Creating ID  " << id << " for strToken " << strToken << std::endl;

    switch (strToken[0]) {
    case 'S':
      // std::cout << "Source\n";
      hops.push_back(Hop(Hop::HopKind::SOURCE));
      break;
    case 'L':
      // std::cout << "Link\n";
      hops.push_back(Hop(Hop::HopKind::LINK, -1));
      break;
    case 'D':
      // std::cout << "Destination\n";
      hops.push_back(Hop(Hop::HopKind::DESTINATION));
      break;
    default:
      // std::cout << "Invalid\n";
      assert(false);
    }} else {
    id = ids.at(strToken);
    // std::cout << "Retrieving ID for " << strToken << ": " << id << std::endl;
    }
  return id;
}

int Workload::parseTmFile(const char* tmfile) {

  std::ifstream inFile(tmfile);
  std::string oneLine;

  std::cout << "parsing tm file " << tmfile << std::endl;
  if (!inFile.is_open()) return 1;

  while(getline(inFile, oneLine)) {
    std::cout << "tokenizing " << oneLine << std::endl;
    char cline[80];
    int size = oneLine.length();
    if (80< size) size = 80;
    std::strncpy(cline, oneLine.c_str(), size);
    char delim[] = " ";
    char *token = NULL;

    std::vector<int> path;

    token = std::strtok(cline, delim);
    std::cout << token << std::endl;
    int id = getId(token);
    path.push_back(id);

    while((token=std::strtok(NULL, delim)) != NULL) {
      std::cout << token << std::endl;
      id = getId(token);
      path.push_back(id);
    }
    flowToPathId.push_back(paths.size());
    paths.push_back(path);
    std::cout << std::endl;    
  }
  inFile.close();

  int flow = 0;
  for (const auto& path : paths) {
    std::cout << "Flow " << flow++ << ":";
    for (int id: path) {
      std::cout << " " << id << " ( " << hops.at(id).getString() << "), ";
    }
    std::cout << std::endl;
  }

  std::cout << "all hops\n";
  int i = 0;
  for (const auto& hop : hops) {
    std::cout << i++ << ": " << hop.getString() << "\n";
  }

  return 0;
}

// int parseTmFile(const char* tmfile) {
//   std::ifstream inFile(tmfile, std::ifstream::in);
//   char oneLine[80];
//   std::cout << "parsing Tm file " << tmfile << std::endl;
//   while(inFile) {
//     inFile.getline(oneLine, 80);
//     std::cout << "tokenizing " << oneLine << std::endl;

//     char delim[] = " ";
//     char *token = NULL;

//     token = strtok(oneLine, delim);
//     std::cout << token << std::endl;

//     while((token=strtok(NULL, delim)) != NULL) {
//       std::cout << token << std::endl;
//     }
//     std::cout << std::endl;    
//   }
//   inFile.close();
//   return 0;
// }



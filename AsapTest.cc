#include "Asap.h"
#include <iostream>

#define NUM_FLOWS 2
#define NUM_HOPS 3
#define LOG 1

int oneLink(double cap, double initRate);

int main() {
  return oneLink(50, 1); 
  // ASAP paper: initRate is 0?
  //  since first FRM sets R = C / N 
  //     (not counting flow as sat, that's why initRate should be 0)
  //      otherwise count flow as sat at initRate
  //   first BRM gets ER = min of the Rs
  //   second FRM gets ACR = ER and old ACR = initRate

  //   second FRM at bn link is const->unconst, set Nu++, R = C/Nu
  //   second FRM at non bn link is const->const, Fc += Acr - oldAcr
  //     and R = C-Fc/Nu .. ideally want Acr - oldAcr to be initRate
  //     so as long as initRate < min of the Rs should be fine?!?  
}

// TODO(lav): generalize to handle any topology, flows
// TODO(lav): add a function to check for convergence
// TODO(lav): handle floating point errors
// TODO(lav): run extensive simulations
// TODO(lav): make similar for other mp schemes
int oneLink(double cap, double initRate) {
  Message message[NUM_FLOWS];
  for (int i = 0; i < NUM_FLOWS; i++) {
    message[i] = Message(i, Message::MessageKind::INIT);
    message[i].init.ICR = initRate; // TODO(lav): initial rate??
  }

  int cycleLength = 2*NUM_HOPS-2;
  Hop* path[2*NUM_HOPS-2];
  Hop source = Hop(Hop::HopKind::SOURCE);
  Hop link = Hop(Hop::HopKind::LINK, cap);
  Hop destination = Hop(Hop::HopKind::DESTINATION);
  path[0] = &source;
  path[1] = &link;
  path[2] = &destination;
  path[3] = &link;

  int flow = 0;
  int nextHop[NUM_FLOWS];
  for (int i = 0; i < NUM_FLOWS; i++) nextHop[i] = 0;

  for (int ev = 0; ev < 30; ev++) {    
    Hop& hop = *path[nextHop[flow]%cycleLength];
    Message& msg = message[flow];

    if (LOG) std::cout << "\n\n";
    if (LOG) std::cout << "Event # " << ev << "\n";

    if (LOG) std::cout << "Input Message ";
    if (LOG) std::cout << message[flow].getString() << "\n";
    if (LOG) std::cout << "Hop (before) " ;
    if (LOG) std::cout << hop.getString() << "\n";

    Events::handleEvent(msg, hop);

    if (LOG) std::cout << "Output Message ";
    if (LOG) std::cout << message[flow].getString() << "\n";
    if (LOG) std::cout << "Hop (after) " ;
    if (LOG) std::cout << hop.getString() << "\n";

    nextHop[flow]++;
    flow = (flow + 1)%NUM_FLOWS;

  }
  
  return 0;
}

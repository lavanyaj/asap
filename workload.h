#ifndef __WORKLOAD_H
#define __WORKLOAD_H

#include "Asap.h" // Hop
#include <vector>

class Workload {
private:
  std::vector<Hop> hops;
  std::vector<std::vector<int> > paths;
  std::vector<int> flowToPathId;

  //std::map<int, Hop> sources;
  //std::map<int, Hop> destinations(Hop::HopKind::Destination);
  std::map<std::string, int> ids;
public:
  std::vector<Hop>& getHops() {
    return hops;
  }

  std::vector<std::vector<int> >& getPaths() {
    return paths;
  }
  std::vector<int>& getFlowToPathId() {
    return flowToPathId;
  }

  int parseTmFile(const char* filename);
  int parseCapFile(const char* filename);
  int getId(const char* token);
};

#endif

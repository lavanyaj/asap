#include "MPSimulation.h"
#include <map>
#include <iostream> // cout
#include <stdlib.h> // srand, rand

#define LOG 1
#define SKIP 10

void MPSimulation::run(int maxSteps, int seed) {
  bool checkConvergence = true;
  bool converged = false;
  int flow = 0;

  if (seed >= 0) srand(seed);
  
  // For checking convergence
  // number of round trips completed by flow
  std::map<int, int> rttsPerFlow;
  // messages indexed by hop and flow, every time
  // "all flows complete a new round trip" i.e.,
  // "min round trips" increases, where "min round trips"
  // is the number of round trips completed by the 
  // slowest flow.
  int numHops = hops.size();
  int numFlows = flowToPath.size();

  int numValidOldMessages = 0;
  int numValidNewMessages = 0;


  std::vector<std::vector<Message> > 
    oldMessages(numHops, std::vector<Message>(numFlows, Message()));

  // each time "all flows complete a new round trip"
  // compare latest messages to previous to see if
  // they've converged

  // Only storing forward messages
  std::vector<std::vector<Message> > 
    newMessages(numHops, std::vector<Message>(numFlows, Message()));

  int minRtts = 0;
  int ev = 0;
  while (ev < maxSteps) {
    if (converged) break;

    if (LOG) std::cout << "\n\n";

    if (LOG) std::cout << " flow " << flow;
    assert(flowNextHop.size() > flow);
    int nextHopCount = flowNextHop.at(flow);    
    if (LOG) std::cout << " nextHopCount " << nextHopCount;
    assert(flowToPath.size() > flow);
    int pathId = flowToPath.at(flow);
    if (LOG) std::cout << " pathId " << pathId;
    assert(paths.size() > pathId);
    const std::vector<int>& path = paths.at(pathId);    
    if (LOG) std::cout << " found_path ";
    assert(path.size() > nextHopCount);
    int nextHopId = path.at(nextHopCount);
    if (LOG) std::cout << " nextHopId " << nextHopId;
    assert(hops.size() > nextHopId);
    Hop& hop = hops.at(nextHopId);
    if (LOG) std::cout << " found_hop ";    
    Message& msg = flowMessage.at(flow);
    if (LOG) std::cout << " found_flowMessage \n";

    if (LOG) std::cout << "Event # " << ev 
		       << " flow id " << flow
		       << " at hop id " << nextHopId
		       << "\n";

    if (LOG) std::cout << "Input Message ";
    if (LOG) std::cout << flowMessage.at(flow).getString() << "\n";
    if (LOG) std::cout << "Hop (before) " ;
    if (LOG) std::cout << hop.getString() << "\n";

    Events::handleEvent(msg, hop);

    if (checkConvergence && flowMessage.at(flow).kind != Message::MessageKind::BRM) {
      if (newMessages.at(nextHopId).at(flow).kind == Message::MessageKind::INVALID)
	numValidNewMessages++;
      newMessages.at(nextHopId).at(flow) = flowMessage.at(flow);
    }
    if (LOG) std::cout << "Output Message ";
    if (LOG) std::cout << flowMessage.at(flow).getString() << "\n";
    if (LOG) std::cout << "Hop (after) " ;
    if (LOG) std::cout << hop.getString() << "\n";
    
    int cycleLength = path.size();
    flowNextHop.at(flow) = (flowNextHop.at(flow)+1)%cycleLength;
    
    if (rttsPerFlow.count(flow) == 0) {
      rttsPerFlow[flow] = 0;
    }

    if (flowNextHop.at(flow) == 0) rttsPerFlow.at(flow)++;

    if (checkConvergence && rttsPerFlow.begin()->second > minRtts) {
      minRtts = rttsPerFlow.begin()->second;
      if (numValidOldMessages > 0) {
	bool identical = true;
	int i = 0; int j = 0;
	while (i < numHops && identical) {
	  while (j < numFlows && identical) {
	    Message& old = oldMessages[i][j];
	    Message& nu = newMessages[i][j];
	    if (old.kind != nu.kind
		|| (old.valid() && !old.equals(nu))) {
	      identical = false;
	      std::cout << "Message of flow " << j << " at hop " << i
			<< " was " << old.getString()
			<< ", not equal to now " << nu.getString()
			<< std::endl;
	    }
	    j++;
	  }
	  i++;
	}
	std::cout << (identical ? "converged" : " not converged yet") 
		  << " after " << minRtts << " RTTs"
		  << std::endl;	
	if (identical) converged = true;
	// check for convergence	  
      }
      numValidOldMessages = numValidNewMessages;
      oldMessages = newMessages;
      // reset newMessages
      newMessages = std::vector<std::vector<Message> > 
	(numHops, std::vector<Message>(numFlows, Message()));
      numValidNewMessages = 0;
    }

    flow = (flow + 1)%numFlows;
    if (seed >= 0 && SKIP && (rand() % SKIP) == 0) {
      if (LOG) std::cout << "\n\n Flow " << flow << " skipped";
      flow = (flow + 1)%numFlows;
    }

    ev++;
  }
  
  return;
} 

std::vector<std::vector<int> > MPSimulation::makeRoundTrip
(const std::vector<std::vector<int> >& paths) {
  std::vector<std::vector<int> > ret;
  for (const auto& path: paths)
    ret.push_back(path);

  for (auto& path: ret) {
    int index = path.size()-2;
    while (index > 0) {
      path.push_back(path.at(index--));
    }
  }

  std::cout << "Round trip of flows\n";

  int flow = 0;
  for (auto& path: ret) {
    std::cout << "Flow " << flow++ << ":"; 
    for (auto& id: path) {
      std::cout << " " << id;
    }
    std::cout << std::endl;
  }

  return ret;
}
					      
  
